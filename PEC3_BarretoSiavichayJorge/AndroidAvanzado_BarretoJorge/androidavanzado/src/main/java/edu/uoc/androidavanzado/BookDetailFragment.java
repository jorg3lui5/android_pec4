package edu.uoc.androidavanzado;


import android.app.Activity;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;

import edu.uoc.androidavanzado.R;
import edu.uoc.androidavanzado.model.BookItem;


/**
 * A simple {@link Fragment} subclass.
 */
public class BookDetailFragment extends Fragment {

    //Declara el argumento de entrada, el cual es el id del elemento del cual se recuperarán los datos
    //public static final String idItem = "idItem";

    //es el parametro o poisicion del libro que recibirá desde la lista de libros cuando se
    //seleccione uno de ellos
    public static final String posicionLibro = "posicionLibro";
    // Declaración del objeto a usar en el Fragment
    // Este objeto tendrá toda la información o detalle que se va a visualizar en pantalla
    private BookItem bookItem;
    private ImageView imagenCabeceraDetalle;


    public BookDetailFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //------ INICIO PEC 1 --------------
        //SI el argumento es el idItem, entonces se cargan los datos y el contenido según este id recibido como parámetro.
//        if (getArguments().containsKey(idItem)) {
//            //Obtiene el item que se encuentra en la posición que indica el argumento recuperado.
//            // despues de obtener el objeto de tipo BookItem, se recupera la propiedad titulo y se visualiza como el titulo de la
//            // pantalla dentro del BarLayout
//            bookItem = BookContent.ITEMS.get(getArguments().getInt(idItem));
//            Activity activity = this.getActivity();
//            CollapsingToolbarLayout appBarLayout = (CollapsingToolbarLayout) activity.findViewById(R.id.toolbarCollapsin);
//            if (appBarLayout != null) {
//                appBarLayout.setStatusBarScrimColor(Color.WHITE);
//                appBarLayout.setTitle(bookItem.title);
//                appBarLayout.setCollapsedTitleTextColor(Color.WHITE);
//                appBarLayout.setExpandedTitleColor(Color.WHITE);
//            }
//        }
        //------ FIN PEC 1 --------------

        //------ INICIO PEC 2 --------------

        //en este metodo, se lee la posición del libro seleccionado y obtiene todo el
        //detalle de ese libro desde la base de datos local.
        if (getArguments().containsKey(posicionLibro)) {
            // ============ INICI CODI A COMPLETAR ===============

            AsyncTask.execute(new Runnable() {
                @Override
                public void run() {
                    BookContent bookContent = (BookContent) getContext().getApplicationContext();
                    //obtiene la lista de libros de la base de datos local y extrae el libro
                    //que se encuentre en la posicion indicada
                    List<BookItem>  listaLibros = bookContent.getBooks();
                    bookItem = listaLibros.get(getArguments().getInt(posicionLibro));

                    getActivity().runOnUiThread(new Runnable() {
                        public void run() {
                            Activity activity = getActivity();
                            CollapsingToolbarLayout appBarLayout = (CollapsingToolbarLayout) activity.findViewById(R.id.toolbarCollapsin);
                            if (appBarLayout != null) {
                                appBarLayout.setStatusBarScrimColor(Color.WHITE);
                                appBarLayout.setTitle(bookItem.title);
                                appBarLayout.setCollapsedTitleTextColor(Color.WHITE);
                                appBarLayout.setExpandedTitleColor(Color.WHITE);
                            }
                            //muestra la imagen del libro en la barra superior o cabecera.
                            imagenCabeceraDetalle = activity.findViewById(R.id.imagen_cabecera_detalle);
                            if (imagenCabeceraDetalle != null) {
                                Picasso.with(getActivity()).load(bookItem.url_image).into(imagenCabeceraDetalle);
                            }
                        }
                    });

                }
            });

            // ============ FI CODI A COMPLETAR ===============

        }
        //------ FIN PEC 2 --------------

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Infla el layout para este fragment.
        // Se recupera el fragment que contiene el detalle del item.
        // En los diferentes elementos del fragment se mostrará el autor, fecha de publicación  y descripción
        // del elemento seleccionado y que se visualiza actualmente.
        View view= inflater.inflate(R.layout.fragment_book_detail, container, false);
        if (bookItem != null) {
            ((TextView) view.findViewById(R.id.txtAutorItem)).setText(bookItem.author);
            ((TextView) view.findViewById(R.id.txtFechaPublicacionItem)).setText(bookItem.publication_date);
            ((TextView) view.findViewById(R.id.txtDescripcionItem)).setText(bookItem.description);
            ImageView imageView1 = (ImageView) view.findViewById(R.id.imgImagenItem);

            if (imagenCabeceraDetalle != null) {
                imageView1.setVisibility(View.GONE);
            } else {
                Picasso.with(getActivity()).load(bookItem.url_image).into(imageView1);
            }
        }
        return view;
    }

}
