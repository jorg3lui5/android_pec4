package edu.uoc.androidavanzado.model;


import android.os.Build;


import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.util.Objects;
//EL objeto representa una tabla en la base de datos local para que se pueda persitir.
@Entity(tableName = "BookItem")
public class BookItem {
    public int identificador;
    //se define la clave primaria de la tabla
    @PrimaryKey
    @NonNull
    public String title;
    public String author;
    public String editorial;
    public String publication_date;
    public String description;
    public String url_image;

    public BookItem() {
    }

    public int getIdentificador() {
        return identificador;
    }

    public void setIdentificador(int identificador) {
        this.identificador = identificador;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getEditorial() {
        return editorial;
    }

    public void setEditorial(String editorial) {
        this.editorial = editorial;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPublication_date() {
        return publication_date;
    }

    public void setPublication_date(String publication_date) {
        this.publication_date = publication_date;
    }

    public String getUrl_image() {
        return url_image;
    }

    public void setUrl_image(String url_image) {
        this.url_image = url_image;
    }

    @Override
    public String toString() {
        return "BookItem{" +
                "identificador=" + identificador +
                ", title='" + title + '\'' +
                ", author='" + author + '\'' +
                ", editorial='" + editorial + '\'' +
                ", publication_date=" + publication_date +
                ", description='" + description + '\'' +
                ", url_image='" + url_image + '\'' +
                '}';
    }



    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public int hashCode() {
        return Objects.hash(title);
    }
}

