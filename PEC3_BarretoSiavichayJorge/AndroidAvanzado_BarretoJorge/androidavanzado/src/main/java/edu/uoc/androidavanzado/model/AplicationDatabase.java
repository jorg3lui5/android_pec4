package edu.uoc.androidavanzado.model;


import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

@Database(entities = {BookItem.class}, version = 1)
/*
Esta clase se usa para aplicar el patrón Singleton para que la clase solo tenga una instancia,
proporcionando un punto de acceso global a ella.
He considerado que la conexión a al base de datos es muy buen ejemplo para la aplicación de este patrón
porque se deberia tener solo una instancia de la conexión y no crear varias conexiones.
* */
public abstract class AplicationDatabase extends RoomDatabase {

    private static final String NOMBRE_BASE_DATOS = "bdLibros";
    //Declara el objeto que tendrá la unica instancia
    private static volatile AplicationDatabase instancia;

    public abstract LibroDao libroDao();

    /*Se debería colocar al contructor como privado para que cumpla adecuadamente con el
    patrón Singleton, pero en ese caso se comentó porque la clase que se genera automáticamente (AplicationDatabase_Impl)
    necesita un constructor por defecto.*/
//    private AplicationDatabase(){
//
//    }


    // Instancia de acceso único
    // Se aplica para que solo exista una instancia de este Objeto. Y si ya existe una
    // instancia, entonces se devuelve esa instancia.
    public static synchronized AplicationDatabase getInstance(Context context) {
        if (instancia == null) {
            instancia = Room.databaseBuilder(context, AplicationDatabase.class, NOMBRE_BASE_DATOS).build();
        }
        return instancia;
    }

}