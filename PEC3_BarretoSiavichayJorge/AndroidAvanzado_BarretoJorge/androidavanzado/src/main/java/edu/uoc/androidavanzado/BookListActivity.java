package edu.uoc.androidavanzado;

import android.annotation.TargetApi;
import android.app.ActivityOptions;
import android.app.AlertDialog;
import android.app.NotificationManager;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.FileProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.initialization.InitializationStatus;
import com.google.android.gms.ads.initialization.OnInitializationCompleteListener;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.GenericTypeIndicator;
import com.google.firebase.database.ValueEventListener;
import com.mikepenz.materialdrawer.AccountHeader;
import com.mikepenz.materialdrawer.AccountHeaderBuilder;
import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.DrawerBuilder;
import com.mikepenz.materialdrawer.model.DividerDrawerItem;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.ProfileDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import edu.uoc.androidavanzado.model.BookItem;


public class BookListActivity extends AppCompatActivity {

    private ListView listView;

    private ArrayAdapter<String> adaptador;

    //public String[] itemsEstaticos = new String[20];

    //El atributo esPantallaGrande sirve para identificar si la App se está ejecutando en una pantalla grande (tablet) o no.
    //Si se muestra en una pantalla grande, entonces se muestra tanto el maestro y el detalle.
    private boolean esPantallaGrande;

    private RecyclerView recyclerViewItems;

    private SimpleItemRecyclerViewAdapter adaptadorRecyclerView;

    //PEC 2: Variables para el uso Firebase
    private FirebaseAuth mAuth;
    private FirebaseDatabase database;
    private DatabaseReference refDatabase;
    private SwipeRefreshLayout swipeRefreshLayout;

    //Variables que identifican las acciones que se podrá realizar sobre según la acción seleccionada por el usuario.
    private static final String ELIMINAR_DE_LISTA = "eliminar_de_lista";
    private static final String VER_DETALLE = "ver_detalle";

    //Variable que identifica la posición en la que se encuentra el libro que se va a eliminar o ver su detalle.
    public String posicionLibro = "posicionLibro";

    //Variables para almacenar la posición del libro y la acción que se realizará sobre el mismo (1. ver detalle, 2 Eliminar)
    int posicionDelLibro=-1;
    int accion=0; //1: Ver detalle, 2: Eliminar.

    //Variable para el manejo de la notificación.
    NotificationManager notificationManager;

    /*
    ----------------------INICIO PEC 4------------------------------
    DECLARACION DE VARIABLES
     */
    //Define el texto que se va a compartir a otras aplicaciones o copiar al portapapeles
    private String textoACompartir= "Aplicación Android sobre libros";
    //Webview que contendrá el formulario de compra del libro.
    FloatingActionButton fab;
    WebView webViewCompra;
    //Ruta del archivo que contiene el formulario de compra
    String urlFormularioCompra = "file:///android_asset/form.html";
    //AdView para mostrar la publicidad
    private AdView mAdView;
    /*
    ----------------------FIN PEC 4------------------------------
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book_list);

        //==========INICIO DE CARGA DE DATOS EN EL LISTVIEW

        // ESTO SE CREÓ INICIALMENTE CUANDO SE UTILIZÓ UN LISTVIEW y AHORA HA SIDO REEMPLAZADO POR UN RECYCLERVIEW

/*        //Crear una lista con 10 elementos estáticos (Item 1, Item 2, ...)
        for (int i=0;i<20;i++){
            itemsEstaticos[i]="Item "+(i+1);
        }
        //Crea un adaptador con los 20 elementos existentes en itemsEstaticos, para luego agregarle al ListView
        ArrayAdapter<String> itemsAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, itemsEstaticos);
        ListView listView = (ListView) this.findViewById(R.id.lstViewListaItems);
        listView.setAdapter(itemsAdapter);*/

        //==========FIN DE CARGA DE DATOS EN EL LISTVIEW

        //Muestra la barra de titulo
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitle(getTitle());
        /*
        ----------------------INICIO PEC 4 - EJERCICIO 1------------------------------
         */
        /*
        Crea la cabecera del menu lateral.
        La cabecera tiene un baclground de una imagen con el nombre "fondo"
        Al cual se le agrega un perfil simulado con un nombre de usuario ("Jorge Barreto"),
        un email ("jorg3lui5@hotmail.com") y una foto que hace referencia a una imagen con nombre "usuario".
        Además agrega 3 opciones: "Compartir con otras aplicaciones", "Copiar en el portapapeles" y
        "Compartir con WhatsApp", con lineas de division entre ellos,
        También implementa el método withOnDrawerItemClickListener, que detecta la selecciónd e una opcion del menu.
         */

        //construye la cabecera
        AccountHeader accountHeader= new AccountHeaderBuilder()
                .withActivity(this)
                .withTranslucentStatusBar(true)
                .withHeaderBackground(R.drawable.fondo)
                .addProfiles(
                        //agrega un nombre de usuario, correo electronico y foto.
                        new ProfileDrawerItem().withName("Jorge Barreto").withEmail("jorg3lui5@hotmail.com").withIcon(getResources().getDrawable(R.drawable.usuario))
                )
                .build();
        //se crean las 3 opciones
        PrimaryDrawerItem opcion1=new PrimaryDrawerItem().withName("Compartir con otras aplicaciones").withIcon(R.drawable.compartir);
        PrimaryDrawerItem opcion2= new PrimaryDrawerItem().withName("Copiar en el portapapeles").withIcon(R.drawable.portapapeles);
        PrimaryDrawerItem opcion3= new PrimaryDrawerItem().withName("Compartir con WhatsApp").withIcon(R.drawable.whastapp);

        //SecondaryDrawerItem subopcion1= new SecondaryDrawerItem().withName("submen").withIcon(FontAwesome.Icon.faw_github);

        //Se construye el menu lateral agregando las 3 opciones y entre ellos una linea de division.
        Drawer drawer= new DrawerBuilder()
                .withActivity(this)
                .withToolbar(toolbar)
                .withAccountHeader(accountHeader)
                .addDrawerItems(
                        opcion1,
                        new DividerDrawerItem(),
                        opcion2,
                        new DividerDrawerItem(),
                        opcion3
                        //subopcion1
                )
                //Método implementado para identificar la selección de las opciónes del menu.
                .withOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() {
                    @Override
                    public boolean onItemClick(View view, int position, IDrawerItem drawerItem) {
                        /*
                        Cada una de las opciones seleccionadas llama a un metodo específico, para realizar
                        la accion especificada. No existe la poisicion 2 y 4 porque esas posiciones hacen
                        referencia a las lineas de division existentes entre las opciones.
                         */
                        if(position==1){
                            compartirConOtrasAplicaciones();
                        }
                        if (position == 3) {
                            copiarEnPortapapeles();
                        }
                            if(position == 5){
                            compartirConWhatsApp();
                        }
                        return true;
                    }
                })
                .build();
        /*
        ----------------------FIN PEC 4 - EJERCICIO 1------------------------------
        */

        /*
        ----------------------INICIO PEC 4 - EJERCICIO 4------------------------------
        NOTA: EL AdView se encuentra dentro de los layout "book_list.xml", y poseen por defecto
        anuncios de prueba debido a que
        aun no se habilitan los anuncios para mi cuenta y proyecto de Google AdMob.
        Sin embargo, están comentados ahí mismo y se puede descomentar en los "book_list.xml" para probarlo.
         */
        //MobileAds.initialize(this, "ca-app-pub-6848274817750251~4226819117");

        //INicializa el SDK de anuncios móviles.
        MobileAds.initialize(this, new OnInitializationCompleteListener() {
            @Override
            public void onInitializationComplete(InitializationStatus initializationStatus) {
            }
        });

        //obtiene el elemento AdView definido en los layout "book_list.xml"
        mAdView = findViewById(R.id.adViewPublicidad);
        //Se carga el anuncio con loadAd() que recibe el parámetro AdRequest
        AdRequest adRequest = new AdRequest.Builder().addTestDevice(AdRequest.DEVICE_ID_EMULATOR).build();
        mAdView.loadAd(adRequest);
         /*
        ----------------------FIN PEC 4 - EJERCICIO 4------------------------------
         */

        /*
        ----------------------INICIO PEC 3------------------------------
         */
        //crea una instancia uqe servirá para eliminar la notificación ya abierta.
        notificationManager= (NotificationManager) getBaseContext().getSystemService(Context.NOTIFICATION_SERVICE);

        //Recupera del Intent la acción seleccionada por el usuario y la posición del libro al cual se le aplicará la acción.
        if (getIntent() != null && getIntent().getAction() != null && getIntent().getExtras()!=null && getIntent().getExtras().containsKey(posicionLibro)) {
            try {
                //convierte a número la posicion del libro.
                posicionDelLibro = Integer.parseInt(getIntent().getExtras().getString(posicionLibro));

                //accion=2 cuando la acción es "eliminar de la lista".
                //accion=1 cuando la acción es "ver detalle".
                //este valor se usará en los métodos obtenerLibrosDesdeBaseLocal() y obtenerLibrosConFormatoDataSnapshot().
                if (getIntent().getAction().equalsIgnoreCase(ELIMINAR_DE_LISTA)) {
                    accion=2;
                } else if (getIntent().getAction().equalsIgnoreCase(VER_DETALLE)) {
                    accion=1;
                }
            }
            catch(NumberFormatException n) {



            }
            finally {
                //Cierra o elimina la notificación seleccionada.
                notificationManager.cancelAll();
            }
        }
        /*
        ----------------------FIN PEC 3------------------------------
        */

        webViewCompra = (WebView) findViewById(R.id.webViewCompra);
        webViewCompra.setVisibility(View.INVISIBLE);


        fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fab.setVisibility(View.INVISIBLE);
                webViewCompra.setVisibility(View.VISIBLE);
                final WebSettings ajustesWebView = webViewCompra.getSettings();
                ajustesWebView.setJavaScriptEnabled(true);
                webViewCompra.loadUrl(urlFormularioCompra);
                webViewCompra.setWebViewClient(new WebViewClient() {

                    @SuppressWarnings("deprecation")
                    @Override
                    public boolean shouldOverrideUrlLoading(WebView view, String url) {
                        Uri uriMetodoGet =Uri.parse("url");
                        validarEnviarFormulario(uriMetodoGet);
                        return true;
                    }

                    @TargetApi(Build.VERSION_CODES.N)
                    @Override
                    public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
                        Uri uriMetodoGet = request.getUrl();
                        validarEnviarFormulario(uriMetodoGet);
                        return true;
                    }

                    private void validarEnviarFormulario(Uri uriMetodoGet){
                        System.out.println(uriMetodoGet);
                        String nombre = uriMetodoGet.getQueryParameter("name");
                        System.out.println(nombre);
                        String numeroTarjeta= uriMetodoGet.getQueryParameter("num");
                        String fechaCaducidad= uriMetodoGet.getQueryParameter("date");
                        if(nombre==null || nombre.equals("")){
                            mostrarMensajeAlerta(BookListActivity.this,"Validación","Debe ingresar el nombre");
                        }
                        else if(numeroTarjeta==null || numeroTarjeta.equals("")){
                            mostrarMensajeAlerta(BookListActivity.this,"Validación","Debe ingresar el número de tarjeta");
                        }
                        else if(fechaCaducidad==null || fechaCaducidad.equals("")){
                            mostrarMensajeAlerta(BookListActivity.this,"Validación","Debe ingresar la fecha de caducidad");
                        }
                        else {
                            SimpleDateFormat simpleDateFormat=new SimpleDateFormat("dd/MM/yyyy");
                            try{
                                Date date1=simpleDateFormat.parse(fechaCaducidad);
                                webViewCompra.setVisibility(View.INVISIBLE);
                                fab.setVisibility(View.VISIBLE);
                                mostrarMensajeAlerta(BookListActivity.this,"Compra","La Compra se ha realizado correctamente");

                            }
                            catch (ParseException e){
                                mostrarMensajeAlerta(BookListActivity.this,"Validación","La fecha debe tener el formato dd/MM/yyyy (Ejm: 23/01/2020)");
                            }

                        }
                    }
                });
//                Snackbar.make(view, "MENSAJE ACIVADO", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
            }
        });

        //---------------------INICIO PEC2 - FIREBASE------------------
        //inicializa clases para autenticacion y accedo a datos del servidor firebase
        mAuth = FirebaseAuth.getInstance();
        database = FirebaseDatabase.getInstance();

        // ============ INICIO CODIGO A COMPLETAR ===============

        //
        // Realiza el login con el servidor de firebase y si el logueo es satisfactorio
        // entonces obtiene la lista de libros existentes en este servidor, caso contrario
        // Muestra el mensaje "Error al loguear".
        //ESte usuario y contraseña está fue creado en el servidor y tiene autorización para acceder a firebase
        mAuth.signInWithEmailAndPassword("androidavanzadopec4@uco.edu.ec", "abc123")
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            obtenerLibros();
                        } else {
                            Log.v("aaaaa",task.getException().toString());
                            Toast.makeText(BookListActivity.this, "Error al loguear, compruebe los datos", Toast.LENGTH_SHORT).show();
                        }
                    }

                });

        // ============ FIN CODIGO A COMPLETAR ==================

        //---------------------FIN PEC2 - FIREBASE------------------


        swipeRefreshLayout = findViewById(R.id.swipeRefreshLayout);
        //Escucha el refresco o actualización de datos para realizar una nueva carga de datos.
        //se refresca la lista de libros al estirar de la lista hacia abajo.
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                // ============ INICIO CODIGO A COMPLETAR ===============
                if (refDatabase != null) {
                    //refresca los datos, obteniendolos del servidor o base local cuando exista
                    //algun error en el mismo.
                    refDatabase.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            //obtiene datos desde el servidor
                            obtenerLibrosConFormatoDataSnapshot(dataSnapshot);
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {
                            // Falla al leer los valores
                            Log.w("BookListActivity", "Error al leer los datos.", databaseError.toException());
                            obtenerLibrosDesdeBaseLocal();
                        }
                    });
                } else {
                    Log.w("BookListActivity", "La referencia a la base de datos es null");
                    obtenerLibrosDesdeBaseLocal();
                }
                // ============ FIN CODIGO A COMPLETAR ===============
            }
        });
        // Configura los colores de la actualización o refresco de datos.
        swipeRefreshLayout.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);



        //------INICIO PEC 1---------------
        //UTILIZANDO RECYCLERVIEW
        //Recupera el recycler view con tal id del xml.
//        recyclerViewItems = (RecyclerView) findViewById(R.id.lstViewListaItems);
//
//        if(this.findViewById(R.id.fragDetalleItem)!=null){
//
//            //Carga en el fragment el detalle del item cuando la pantalla es grande (se ejecuta en una tablet).
//            RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
//            recyclerViewItems.setLayoutManager(layoutManager);
//
//            //La variable que define si se ejecuta en una tablet(w900dp), entonces se coloca true, con
//            //lo cual se confirma de que se está ejectuando en una tablet
//            esPantallaGrande=true;
//        }
//        else{
//            //Se usó cuando se cargó los elementos en el ListView,  pues le pasa un valor de 1
//            //para que los elementos aparezcan en la lista uno debajo de otro, es decir en una sola columna
//            //StaggeredGridLayoutManager staggeredGridLayoutManager = new StaggeredGridLayoutManager(1, StaggeredGridLayoutManager.VERTICAL);
//
//            //Se configuró para que los items del RecyclerView aparezcan en 2 columnas, se modifica el primer parametro y se coloca el numero 2
//            StaggeredGridLayoutManager staggeredGridLayoutManager = new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL);
//            recyclerViewItems.setLayoutManager(staggeredGridLayoutManager);
//        }
//        setupRecyclerView(recyclerViewItems);
        //------FIN PEC 1---------------

        //------INICIO PEC 2---------------
        //establece el recicler view en donde se llenará los diferentes libros desde el servidor o base de datos local
        recyclerViewItems = (RecyclerView) findViewById(R.id.lstViewListaItems);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerViewItems.setLayoutManager(layoutManager);
        setupRecyclerView(recyclerViewItems);
        if(this.findViewById(R.id.fragDetalleItem)!=null){

            //La variable que define si se ejecuta en una tablet(w900dp), entonces se coloca true, con
            //lo cual se confirma de que se está ejectuando en una tablet
            esPantallaGrande=true;
        }
        //------FIN PEC 2---------------

    }

    /*
    ----------------------INICIO PEC 4 - EJERCICIO 2------------------------------
     */
    /*
    Metodo que comparte el texto “Aplicación Android sobre libros” y el ícono actual de la
    aplicacion a cualquier aplicacion que tenga el usuario disponible.
    Primero se guarda la imagen en el almacenamiento local y despues la comparte.
     */
    private void compartirConOtrasAplicaciones() {
        //Crea el intent con la accion "ACTION_SEND", que es utilizada para compartir o enviar datos
        //a otras aplicaciones.
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        //agrega el texto a compartir que es del tipo texto plano.
        sendIntent.putExtra(Intent.EXTRA_TEXT, textoACompartir);
        sendIntent.setType("text/plain");

        //Obtiene la imagen o icono de la aplicacion.
        Bitmap bmp = BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher);
        //Tranforma y comprime la imagen en un array de bytes para poder escribir en la memoria interna.
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.PNG, 100, stream);
        byte[] byteArray = stream.toByteArray();
        //define el nombre de la carpeta ("comparte_imagen") que contendrá la imagen y la crea en la memoria interna
        File urlCarpeta = new File(getFilesDir(), "comparte_imagen");
        urlCarpeta.mkdir();
        //Define el nombre de la imagen ("imagen_compartir.png") que se guardará dentro de la carpeta creada.
        File urlImagen = new File(urlCarpeta.getPath(), "imagen_compartir.png");
        try {
            //Escribe la imagen en la ruta especificada. Se crea el archivo en la memoria interna
            FileOutputStream outputStream = new FileOutputStream(urlImagen);
            outputStream.write(byteArray);
            outputStream.close();
//            Uri urlImagenCompartir = Uri.parse(getApplicationContext().getFilesDir().getPath() + "/imagen_compartir.png");
//            Uri urlImagenCompartir  = Uri.fromFile(imagen);
            //Obtiene la url de la imagen que se encuientra en la memoria interna
            Uri urlImagenCompartir= FileProvider.getUriForFile(getApplicationContext(), getPackageName(), urlImagen);

            sendIntent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            // Agrega la url de la imagen como un extra del intent y lo coloca de tipo imagen para poder compartir
            //con las demas aplicaciones.
            sendIntent.putExtra(Intent.EXTRA_STREAM, urlImagenCompartir);
            sendIntent.setType("image/*");
            //abre las aplicaciones que posee el dispositivo para compartir el texto y la imagen
            //que estan dentro del intent
            startActivity(Intent.createChooser(sendIntent, "Compartir con otras aplicaciones"));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e2) {
            e2.printStackTrace();
        }
        catch (Exception e3){
            e3.printStackTrace();
        }
    }
    /*
        Método utilizado para copiar el texto “Aplicación Android sobre libros” en el portapapeles.
        Una vez copiado el texto, muestra una alerta que informa sobre la copia del mismo.
     */
    private void copiarEnPortapapeles() {
        //obtiene un identificador para el servicio de portapapeles
        ClipboardManager clipboard = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
        //Copia el texto a compartir a un nuevo objeto ClipData
        ClipData clip = ClipData.newPlainText("simple text", this.textoACompartir);
        //coloca el texto en el portapapeles
        clipboard.setPrimaryClip(clip);

        //llama a un metodo que muestra la alerta con el título "TEXTO COPIADO" y el mensaje “Aplicación Android sobre libros”
        this.mostrarMensajeAlerta(this,"TEXTO COPIADO",this.textoACompartir);

    }
    /*
        Metodo que comparte el texto “Aplicación Android sobre libros” y el ícono actual de la
        aplicacion solo a whatsApp.
        Primero se guarda la imagen en el almacenamiento local y despues la comparte.
     */
    private void compartirConWhatsApp() {
        //Se define el nombre del paquete (aplicacion) que debe estar instalado (WhatsApp).
        //verifica que esté instalada la aplicacion y retorna un valor booleano.
        String paqueteWhatsapp= "com.whatsapp";
        //instancia del manejador de paquetes
        PackageManager pm = this.getPackageManager();
        boolean whatsAppInstalado=false;
        try {
            //comprueba si está instalada un paquete con el nombre "com.whatsapp".
            //si salta el error NameNotFoundException, entocnes dicha aplicacion no está instalada.
            pm.getPackageInfo(paqueteWhatsapp, PackageManager.GET_ACTIVITIES);
            whatsAppInstalado= true;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        //Si la aplicación de WhatsApp esté instalada, entocnes guarda la imagen en el almacenamiento interno y la
        //comparte.
        if(whatsAppInstalado==true){
            //Crea el intent con la accion "ACTION_SEND", que es utilizada para compartir o enviar datos
            //mediante la aplicacion de whatsApp.
            Intent sendIntent = new Intent();
            sendIntent.setAction(Intent.ACTION_SEND);
            //agrega el texto a compartir que es del tipo texto plano.
            sendIntent.putExtra(Intent.EXTRA_TEXT, textoACompartir);
            sendIntent.setType("text/plain");
            //Obtiene la imagen o icono de la aplicacion.
            Bitmap bmp = BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher);
            //Tranforma y comprime la imagen en un array de bytes para poder escribir en la memoria interna.
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            bmp.compress(Bitmap.CompressFormat.PNG, 100, stream);
            byte[] byteArray = stream.toByteArray();
            //define el nombre de la carpeta ("comparte_imagen") que contendrá la imagen y la crea en la memoria interna
            File urlCarpeta = new File(getFilesDir(), "comparte_imagen");
            urlCarpeta.mkdir();
            //Define el nombre de la imagen ("imagen_compartir.png") que se guardará dentro de la carpeta creada.
            File urlImagen = new File(urlCarpeta.getPath(), "imagen_compartir.png");
            try {
                //Escribe la imagen en la ruta especificada. Se crea el archivo en la memoria interna
                FileOutputStream outputStream = new FileOutputStream(urlImagen);
                outputStream.write(byteArray);
                outputStream.close();
//                Uri urlImagenCompartir = Uri.parse(getApplicationContext().getFilesDir().getPath() + "/imagen_compartir.png");
//                Uri urlImagenCompartir  = Uri.fromFile(imagen);
                //Obtiene la url de la imagen que se encuientra en la memoria interna
                Uri urlImagenCompartir= FileProvider.getUriForFile(getApplicationContext(), getPackageName(), urlImagen);

                sendIntent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                //Establece que se abra y comparta solo con la aplicaciónd de whatsApp
                sendIntent.setPackage("com.whatsapp");
                // Agrega la url de la imagen como un extra del intent y lo coloca de tipo imagen para poder compartir
                //con las demas aplicaciones.
                sendIntent.putExtra(Intent.EXTRA_STREAM, urlImagenCompartir);
                sendIntent.setType("image/*");
                //comparte el texto y la imagen que estan dentro del intent
                startActivity(sendIntent);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e2) {
                e2.printStackTrace();
            }
            catch (Exception e3){
                e3.printStackTrace();
            }
        }


    }
    /*
    Metodo utilizado para mostrar las alertas en la aplicacion.
    Recibe como parametros: el contexto, el titulo y mensaje de la alerta.
     */
    public void mostrarMensajeAlerta(Context context, String titulo, String mensaje){
        //construye la alerta
        AlertDialog.Builder builder = new AlertDialog.Builder(context);

        //agrega el titulo y el mensaje a mostrar
        builder.setMessage(mensaje)
                .setTitle(titulo);

        //crea un boton "aceptar" para ocultar la alerta
        builder.setPositiveButton("ACEPTAR", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
            }
        });
        //crea y muestra la alerta.
        AlertDialog alertDialog=builder.create();
        alertDialog.show();
    }
    //------FIN PEC 4 - EJERCICIO 2---------------


    //Mostrar el detalle del libro que se encuentra en la base de datos en la posición pasada como parámetro. Este código es el mismo que se usa cuando se da clic en un libro de la lista visualizada en pantalla.
    public void mostrarDetalleDelLibro(int posicionDelLibro){
            if (esPantallaGrande) {
                // ============ INICIO CODIGO A COMPLETAR ===============
                // Iniciar el fragmento correspondiente a tableta, enviando el argumento de la posición seleccionada
                Bundle arguments = new Bundle();
                arguments.putInt(BookDetailFragment.posicionLibro, posicionDelLibro);
                BookDetailFragment fragment = new BookDetailFragment();
                fragment.setArguments(arguments);
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.fragDetalleItem, fragment)
                        .commit();
                // ============ FIN CODIGO A COMPLETAR =================
            } else {
                // ============ INICIO CODIGO A COMPLETAR ===============
                // Iniciar la actividad correspondiente a móvil, enviando el argumento de la posición seleccionada


                //establece la animación para la transición entre la lista de libros y el detalle del libro.
                //Para que cuando se vaya a visualizar el detalle, esta pantalla aparezca de abajo hacia arriba.
                Bundle animacionPantalla = ActivityOptions.makeCustomAnimation(BookListActivity.this, R.anim.trasladar_from_y_delta_100, R.anim.trasladar_from_y_delta_0).toBundle();

                //pasa la posicion del libro mediante un intent, además se incluye la animacion
                Intent intent = new Intent(BookListActivity.this, BookDetailActivity.class);
                intent.putExtra(BookDetailFragment.posicionLibro, posicionDelLibro);
                BookListActivity.this.startActivity(intent, animacionPantalla);
                // ============ FIN CODIGO A COMPLETAR =================
            }
            this.posicionDelLibro=-1;
    }

    //Este método es usado para obtener la lista de libros, los que se mostraran en el reciclerView.
    //Primero se conecta al servidor y desde esa base remota obtiene la lista de libros.
    //Si se cancela o existe algun error de conexión, entonces recupera los libros desde la base de datos local
    private void obtenerLibros() {
        swipeRefreshLayout.setRefreshing(true);

        //Hace referencia a la base de datos del servidor de donde se extraerán los datos.
        refDatabase = database.getReference("books");
        // Lee desde la base de datos, extrayendo los libros cada vez que se produzcan cambios
        refDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                //Cada cambio que se produzca, volverá a cargar la lista de libros.
                obtenerLibrosConFormatoDataSnapshot(dataSnapshot);
            }

            //si se cancela la extracción de datos, entonces los obtiene desde la base local
            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
                Log.w("BookListActivity", "Error al leer los valores desde el servidor.", error.toException());
                obtenerLibrosDesdeBaseLocal();
            }
        });

        //comprueba si existe conexion con el servidor obteniendo una referencia a ".info/connected".
        //Si no existe conexion, carga la lista de libros de la base de datos local.
        DatabaseReference databaseReference = database.getReference(".info/connected");
        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                boolean existeConexion = snapshot.getValue(Boolean.class);
                if (!existeConexion) {
                    //obtiene de la base de datos local
                    obtenerLibrosDesdeBaseLocal();
                }
            }

            //si se cancela el thread, entonces se imprime un error en consola
            @Override
            public void onCancelled(DatabaseError error) {
                Log.w("BookListActivity", "El Listener fue cancelado");
            }
        });

    }

    //el método obtiene los libros de la base de datos local y los agrega al adapter. Además
    // muestra un mensaje en un Snackbar indicando que no hay conexion y que se extrayeron los libros de la
    //base de datos local
    private void obtenerLibrosDesdeBaseLocal() {
        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {
                BookContent bookContent = (BookContent) getApplicationContext();
                final List<BookItem> lstLibros = bookContent.getBooks();

                /*
                ----------------------INICIO PEC 3------------------------------
                 */
                /*Verifica que la lista de libros recuperada de la base de datos local tenga datos y que la posición del libro pasada como parámetro sea mayor o igual a cero y menor que la cantidad de libros recuperados.
                Si la posición es aceptable entonces se procede a realizar la acción seleccionada (1. ver detalle, 2. eliminar).
                Si la acción es eliminar (2), entonces elimina el libro que se encuentra en esa posicion tanto de la lista como de la base de datos y establece la posición del libro con un valor de "-1", para no eliminar otro libro
                Si la acción es ver detalle (1), entonces llama a un método que muestra la actividad o fragment con el detalle del libro.
                */
                if(lstLibros!=null && lstLibros.size()>0 && posicionDelLibro>=0 && posicionDelLibro<lstLibros.size()){
                    if(accion==2){
                        bookContent.deleteBook(lstLibros.get(posicionDelLibro));
                        lstLibros.remove(posicionDelLibro);
                        posicionDelLibro=-1;
                    }
                    else if(accion==1){
                        mostrarDetalleDelLibro(posicionDelLibro);
                    }

                }
                /*
                ----------------------FIN PEC 3------------------------------
                 */
                BookListActivity.this.runOnUiThread(new Runnable() {
                    public void run() {
                        adaptadorRecyclerView.setItems(lstLibros);
                        swipeRefreshLayout.setRefreshing(false);
                        Snackbar.make(swipeRefreshLayout, "No hay conexión a internet. Los libros se han cargado de la base local", Snackbar.LENGTH_LONG).show();
                    }
                });

            }
        });

    }
    // Este método se llama incialmente y cuando se actualicen los datos. Pues se comunica con
    //el servidor para obtener la lista de libros.

    private void obtenerLibrosConFormatoDataSnapshot(DataSnapshot dataSnapshot) {
        //la lista obtenida desde el firebase están en formato DataSnapshot, asi que se converte
        //el tipo de datos recibido a una lista de libros.
        GenericTypeIndicator<ArrayList<BookItem>> lstLibrosGenericTypeIndicator = new GenericTypeIndicator<ArrayList<BookItem>>() {};

        final ArrayList<BookItem> lstLibros = dataSnapshot.getValue(lstLibrosGenericTypeIndicator);

        /*
        ----------------------INICIO PEC 3------------------------------
        */
        /*Verifica que la lista de libros recuperada del servidor tenga datos y que la posición del libro pasada como parámetro sea mayor o igual a cero y menor que la cantidad de libros recuperados y la acción es igual a 2 (eliminar)
        Si se cumple, entonces elimina el libro que sencuentra en esa posición en la lista (así ya no se almacenará en la base de datos local).
        */
        if(lstLibros!=null && lstLibros.size()>0 && posicionDelLibro>=0 && posicionDelLibro<lstLibros.size() && accion==2){
            lstLibros.remove(posicionDelLibro);
            posicionDelLibro=-1;
        }
        /*
        ----------------------FIN PEC 3------------------------------
         */
        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {
                BookContent bookContent = (BookContent) getApplicationContext();
                //Se crea un bucle para recorrer la lista de libros obtenidos desde el servidor.
                //Por cada libro se verifica si este existe en la base de datos local y en el caso
                //de no existir lo guarda en la base de datos .
                if(lstLibros!=null && lstLibros.size()>0){
                    for (int i=0; i<lstLibros.size();i++) {
                        if (!bookContent.exists(lstLibros.get(i))) {
                            bookContent.insertBook(lstLibros.get(i));
                        }
                        /*
                        ----------------------INICIO PEC 3------------------------------
                         */
                        /*
                        Si la posición del libro es igual la posición del libro actual del bucle y si la acción es igual a 1 (ver detalle),
                        entonces llama a un método que muestra la actividad o fragment con el detalle del libro de esa posición.
                         */
                        if(posicionDelLibro==i && accion==1){
                            mostrarDetalleDelLibro(posicionDelLibro);
                            break;
                        }
                        /*
                        ----------------------FIN PEC 3------------------------------
                         */
                    }
                }

            }
        });

        //Se actualiza el adaptador y se muestra un mensaje, indicando que se actualizó la lista de libros
        adaptadorRecyclerView.setItems(lstLibros);
        swipeRefreshLayout.setRefreshing(false);
        Snackbar.make(swipeRefreshLayout, "Los libros fueron actualizados desde el servidor", Snackbar.LENGTH_LONG).show();
    }

    private void setupRecyclerView(@NonNull RecyclerView recyclerView) {
        //PEC 1:
        //adaptadorRecyclerView = new SimpleItemRecyclerViewAdapter(BookContent.ITEMS, this);

        //PEC 2: el adaptador tendrá una lista de objetos de tipo BookItem
        adaptadorRecyclerView = new SimpleItemRecyclerViewAdapter(new ArrayList<BookItem>());

        recyclerView.setAdapter(adaptadorRecyclerView);
    }

    //adapter para la lista de libros a visualizar
    public class SimpleItemRecyclerViewAdapter extends RecyclerView.Adapter<SimpleItemRecyclerViewAdapter.ViewHolder> {

        private List<BookItem> lstBookItem;
        private final static int EVEN = 0;
        private final static int ODD = 1;

        public SimpleItemRecyclerViewAdapter(List<BookItem> lstBookItem) {
            this.lstBookItem = lstBookItem;
        }

        //Actualiza la lista con los datos recibidos del servidor o de la base de datos local
        public void  setItems(List<BookItem> items) {
            // ============ INICIO CODIGO A COMPLETAR ===============
            this.lstBookItem = items;
            notifyDataSetChanged();
            // ============ FIN CODIGO A COMPLETAR ==================
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = null;
            // ============ INICIO CODIGO A COMPLETAR ===============

            if (viewType == EVEN) {
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.elementos_lista, parent, false);
            } else if (viewType == ODD) {
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.elementos_lista_impares, parent, false);
            }
            // ============ FIN CODIGO A COMPLETAR =================
            return new ViewHolder(view);
        }



        //devuelve el tipo par o impar según su posición
        @Override
        public int getItemViewType(int position) {
            int type;
            // ============ INICIO CODIGO A COMPLETAR ===============
                if (position % 2 == 0) {
                    type = EVEN;
                } else {
                    type = ODD;
                }
            // ============ FIN CODIGO A COMPLETAR =================
            return type;
        }

        @Override
        public void onBindViewHolder(final ViewHolder holder, final int position) {
            holder.mItem = lstBookItem.get(position);
            holder.txtTitulo.setText(lstBookItem.get(position).title);
            holder.txtAutor.setText(lstBookItem.get(position).author);
            // ============ INICIO CODIGO A COMPLETAR -PEC1 ===============

            // Guarda la posición actual en la vista del holder
            holder.view.setTag(position);

            // ============ FIN CODIGO A COMPLETAR -PEC1 =================


            // ============ INICIO CODIGO A COMPLETAR - PEC2 ==============
            //cuando se presione en un elemento (libro) del recicler view se obtiene
            //la posicion de dicho libro y se la pasa a la actividad del detalle a través
            // de un Intent
            holder.view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int currentPos=position;
                    // ============ FIN CODIGO A COMPLETAR =================

                    if (esPantallaGrande) {
                        // ============ INICIO CODIGO A COMPLETAR ===============
                        // Iniciar el fragmento correspondiente a tableta, enviando el argumento de la posición seleccionada
                        Bundle arguments = new Bundle();
                        arguments.putInt(BookDetailFragment.posicionLibro, currentPos);
                        BookDetailFragment fragment = new BookDetailFragment();
                        fragment.setArguments(arguments);
                        getSupportFragmentManager().beginTransaction()
                                .replace(R.id.fragDetalleItem, fragment)
                                .commit();
                        // ============ FIN CODIGO A COMPLETAR =================
                    } else {
                        // ============ INICIO CODIGO A COMPLETAR ===============
                        // Iniciar la actividad correspondiente a móvil, enviando el argumento de la posición seleccionada
                        Context context = v.getContext();

                        //establece la animación para la transición entre la lista de libros y el detalle del libro.
                        //Para que cuando se vaya a visualizar el detalle, esta pantalla aparezca de abajo hacia arriba.
                        Bundle animacionPantalla = ActivityOptions.makeCustomAnimation(BookListActivity.this, R.anim.trasladar_from_y_delta_100, R.anim.trasladar_from_y_delta_0).toBundle();

                        //pasa la posicion del libro mediante un intent, además se incluye la animacion
                        Intent intent = new Intent(context, BookDetailActivity.class);
                        intent.putExtra(BookDetailFragment.posicionLibro, currentPos);
                        context.startActivity(intent, animacionPantalla);
                        // ============ FIN CODIGO A COMPLETAR =================

                    }
                }
            });
            // ============ FIN CODIGO A COMPLETAR - PEC2 ===============

        }



        @Override
        public int getItemCount() {
            return lstBookItem.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            public final View view;
            public final TextView txtTitulo;
            public final TextView txtAutor;
            //public final ImageView imgPortada;
            public BookItem mItem;

            public ViewHolder(View view) {
                super(view);
                this.view = view;
                this.txtTitulo = (TextView) view.findViewById(R.id.txtTitulo);
                this.txtAutor = (TextView) view.findViewById(R.id.txtAutor);
                //this.imgPortada = (ImageView) view.findViewById(R.id.imgPortada);
            }

            @Override
            public String toString() {
                return super.toString() + " '" + txtTitulo.getText() + "'";
            }
        }
    }
}
