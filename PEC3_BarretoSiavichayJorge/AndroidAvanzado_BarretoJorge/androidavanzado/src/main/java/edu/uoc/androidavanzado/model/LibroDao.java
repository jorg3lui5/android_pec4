package edu.uoc.androidavanzado.model;



import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import java.util.List;
//Dao para establecer las consultas que servirán para obtener y guardar libros de la base de datos local
@Dao
public interface LibroDao {

    //recupera un libro cuyo identificador sea igual al pasado como parámetro
    @Query("SELECT * FROM BookItem WHERE identificador LIKE :identificador LIMIT 1")
    BookItem buscarPorIdentificador(Integer identificador);

    //recupera un libro cuyo titulo sea igual al pasado como parámetro
    @Query("SELECT * FROM BookItem WHERE title LIKE :titulo LIMIT 1")
    BookItem buscarPorTitulo(String titulo);

    //Recupera todos los libros almacenados localmente
    @Query("SELECT * FROM BookItem")
    List<BookItem> listarTodos();

    //Permite inserta un nuevo libro.
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long insertar(BookItem bookItem);

    @Delete
    void delete(BookItem bookItem);

}
