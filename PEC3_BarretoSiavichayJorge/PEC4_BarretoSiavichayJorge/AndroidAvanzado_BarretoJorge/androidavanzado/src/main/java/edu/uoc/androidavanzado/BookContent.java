package edu.uoc.androidavanzado;

import android.app.Application;

import androidx.room.Room;

import com.facebook.stetho.Stetho;

import java.util.List;
import java.util.Objects;

import edu.uoc.androidavanzado.model.AplicationDatabase;
import edu.uoc.androidavanzado.model.BookItem;
//Mediante esta clase se realiza las peticiones  a la base de datos.
public class BookContent extends Application {


    //public static final List<BookItem> ITEMS = new ArrayList<>();
//    static {
//        BookItem book1 = new BookItem(0, "Title1", "Author1", "editorial1",new Date(), "Description 1", null);
//        BookItem book2 = new BookItem(1, "Title2", "Author2", "editorial2",new Date(), "Description 2", null);
//        BookItem book3 = new BookItem(2, "Title3", "Author3", "editorial3",new Date(), "Description 3", null);
//        BookItem book4 = new BookItem(3, "Title4", "Author4", "editorial4",new Date(), "Description 4", null);
//        BookItem book5 = new BookItem(4, "Title5", "Author5", "editorial5",new Date(), "Description 5", null);
//        BookItem book6 = new BookItem(5, "Title6", "Author6", "editorial6",new Date(), "Description 6", null);
//        BookItem book7 = new BookItem(6, "Title7", "Author7", "editorial7",new Date(), "Description 7", null);
//        BookItem book8 = new BookItem(7, "Title8", "Author8", "editorial8",new Date(), "Description 8", null);
//        BookItem book9 = new BookItem(8, "Title9", "Author9", "editorial9",new Date(), "Description 9", null);
//        BookItem book10 = new BookItem(9, "Title10", "Author10", "editorial10",new Date(), "Description 10", null);
//        BookItem book11 = new BookItem(10, "Title11", "Author11", "editorial11",new Date(), "Description 11", null);
//
//        ITEMS.add(book1);
//        ITEMS.add(book2);
//        ITEMS.add(book3);
//        ITEMS.add(book4);
//        ITEMS.add(book5);
//        ITEMS.add(book6);
//        ITEMS.add(book7);
//        ITEMS.add(book8);
//        ITEMS.add(book9);
//        ITEMS.add(book10);
//        ITEMS.add(book11);
//    }º



    //PEC 2
    private AplicationDatabase aplicationDatabase;

    //se hace referencia a la base de datos local y se implementan los métodos para trabajar con
    //esos datos.
    public void onCreate() {
        super.onCreate();
        Stetho.initializeWithDefaults(this);
        //aquí obtiene la única instancia de conexion a la base de datos mediante el patrón Singleton.
        //A partir de esta única instancia se pueden insertar, leer, eliminar y actualizar los registors de la base de datos.
        aplicationDatabase = AplicationDatabase.getInstance(getApplicationContext());
    }

    //obtiene todos los libros almacenados en la base de datos.
    public List<BookItem> getBooks(){
        // ============ INICIO CODIGO A COMPLETAR ===============
        return aplicationDatabase.libroDao().listarTodos();
        // ============ FIN CODIGO A COMPLETAR ==================
    }

    //verifica si un libro existe en la base de datos.
    //La comparación se hace mediante el titulo del libro.
    public boolean exists(BookItem bookItem) {
        // ============ INICIO CODIGO A COMPLETAR ===============
        BookItem libro = aplicationDatabase.libroDao().buscarPorTitulo(bookItem.getTitle());
        return libro!=null? true: false;
        // ============ FIN CODIGO A COMPLETAR ===============
    }

    //permite insertar un nuevo libro en la base de datos local.
    public void insertBook(BookItem bookItem) {
        // ============ INICIO CODI A COMPLETAR ===============
        aplicationDatabase.libroDao().insertar(bookItem);
        // ============ FIN CODIGO A COMPLETAR ===============
    }

    //permite eliminar un libro en la base de datos local.
    public void deleteBook(BookItem bookItem) {
        // ============ INICIO CODI A COMPLETAR ===============
        aplicationDatabase.libroDao().delete(bookItem);
        // ============ FIN CODIGO A COMPLETAR ===============
    }

}


