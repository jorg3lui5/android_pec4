package edu.uoc.androidavanzado;


import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;


public class BookDetailActivity extends AppCompatActivity {

    /*
    Delcara las varibales para el WebView, la ruta del archivo que contiene el formulario de compra
    y el boton flotante al cual se da clic para que aparezca el formulario.
     */
    WebView webViewCompra;
    String urlFormularioCompra = "file:///android_asset/form.html";
    FloatingActionButton fab;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book_detail);



        //Muestra la barra de herramientas
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitleTextColor(Color.WHITE);

        /*
        ----------------------INICIO PEC 4 - EJERCICIO 3------------------------------
        NOTA: EL formulario tambien se puede probar desde la pantalla principal (Lista de libros).
        Se debe dar clic en el boton flotante que se encuentra en la parte inferior de esa pantalla.
        Se implementó ahi tambien porque a veces el servicio de firebase no está funcionando y logueando
        debido a un bug, entonces si eso sucede, no se podrá acceder al detalle del libro. Por esa razón lo implementé
        tambien en la pantalla principal.
         */

        //Obtiene el elemento webView que contendrá el formulario de compra.
        webViewCompra = (WebView) findViewById(R.id.webViewCompra);
        //Oculta el webview
        webViewCompra.setVisibility(View.INVISIBLE);

        //Muestra el Botón flotante
        fab = (FloatingActionButton) findViewById(R.id.buttonAgregar);
        fab.setOnClickListener(new View.OnClickListener() {
        /*
        Al hacer clic en el boton flotante, se muestra el formulario de compra que está embebido dentro
        del webView. Ademas se crea un cliente WebView que intercepta cuando se intenta enviar los datos
        del formulario a traves de un metodo GET.
        Con la información que se va a enviar, se valida que los campos esten llenos y que la fecha tenga el
        formato "dd/MM/yyyy".
        */
        @Override
        public void onClick(View view) {
            //Se oculta el boton y se muestra el formulario de compra.
            fab.setVisibility(View.INVISIBLE);
            webViewCompra.setVisibility(View.VISIBLE);
            //Permite que se ejecute javascript si el formulario embebido lo usa.
            final WebSettings ajustesWebView = webViewCompra.getSettings();
            ajustesWebView.setJavaScriptEnabled(true);
            //carga el archivo que contiene el formulario de compra.
            webViewCompra.loadUrl(urlFormularioCompra);
            //crea EL CLIENTE WEBVIEW
            webViewCompra.setWebViewClient(new WebViewClient() {

                //Se crean 2 metodos, debido a que en algunos dispositivos no funciona porque está
                //obsoleto. Asi que se implementó el método shouldOverrideUrlLoading para las diferentes versiones.
                @SuppressWarnings("deprecation")
                @Override
                public boolean shouldOverrideUrlLoading(WebView view, String url) {
                    //Obtienen la Uri del metodo GET y llama al metodo que valida los campos del formulario
                    Uri uriMetodoGet =Uri.parse("url");
                    validarEnviarFormulario(uriMetodoGet);
                    return true;
                }

                @TargetApi(Build.VERSION_CODES.N)
                @Override
                public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
                    //Obtienen la Uri del metodo GET y llama al metodo que valida los campos del formulario
                    Uri uriMetodoGet = request.getUrl();
                    validarEnviarFormulario(uriMetodoGet);
                    return true;
                }

                /*
                Metodo que valida los campos llenos en el formulario y muestra alertas si no cumplen con las validaciones
                realizadas. Si los campos y valores son válidos, entoces muestra una alerta con el mensaje "La Compra se ha realizado correctamente"
                y se oculta el formulario de compra y muestra nuevamente el detalle del libro con el boton flotante.
                 */
                private void validarEnviarFormulario(Uri uriMetodoGet){
                    System.out.println(uriMetodoGet);
                    //obtiene el valor del parameto name pasado en la Uri
                    String nombre = uriMetodoGet.getQueryParameter("name");
                    System.out.println(nombre);
                    //obtiene el valor del parameto num pasado en la Uri
                    String numeroTarjeta= uriMetodoGet.getQueryParameter("num");
                    //obtiene el valor del parameto date pasado en la Uri
                    String fechaCaducidad= uriMetodoGet.getQueryParameter("date");
                    //muestra una alerta si el campo nombre no se ha llenado
                    if(nombre==null || nombre.equals("")){
                        mostrarMensajeAlerta(BookDetailActivity.this,"Validación","Debe ingresar el nombre");
                    }
                    //muestra una alerta si el numero de tarjeta nombre no se ha llenado
                    else if(numeroTarjeta==null || numeroTarjeta.equals("")){
                        mostrarMensajeAlerta(BookDetailActivity.this,"Validación","Debe ingresar el número de tarjeta");
                    }
                    //muestra una alerta si la fecha de caducidad no se ha llenado
                    else if(fechaCaducidad==null || fechaCaducidad.equals("")){
                        mostrarMensajeAlerta(BookDetailActivity.this,"Validación","Debe ingresar la fecha de caducidad");
                    }
                    else {
                        SimpleDateFormat simpleDateFormat=new SimpleDateFormat("dd/MM/yyyy");
                        try{
                            //convierte el valor ingresado en el campo fecha y lo trata de convertir al
                            //en un objeto de tipo DATE siempre y cuando tenga el formato dd/MM/yyyy
                            //Si el formato es correcto, muestra el boton flotante y oculta el formulario
                            Date date1=simpleDateFormat.parse(fechaCaducidad);
                            //oculta el formulario
                            webViewCompra.setVisibility(View.INVISIBLE);
                            fab.setVisibility(View.VISIBLE);
                            //Muestra un mensaje satisfactorio de la compra realizada
                            mostrarMensajeAlerta(BookDetailActivity.this,"Compra","La Compra se ha realizado correctamente");

                        }
                        catch (ParseException e){
                            //muestra una alerta si la fecha de caducidad no tiene el formato dd/MM/yyyy
                            mostrarMensajeAlerta(BookDetailActivity.this,"Validación","La fecha debe tener el formato dd/MM/yyyy (Ejm: 23/01/2020)");
                        }

                    }
                }
            });
//                Snackbar.make(view, "MENSAJE ACIVADO", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
        }

        });
        /*
        ----------------------FIN PEC 4 - EJERCICIO 3------------------------------
         */
        // Muestra el boton superior del action Bar
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
/*
        Se pregunta si hay un estado del fragmento guardado o si contiene el fragmento, Y solo
        cuando no lo tenga cargado, procederá a agregarlo al contenedor, caso contrario se agregará
        automáticamente in tener que volver a cargar manualmente.
         */
        if (savedInstanceState == null) {
            //Aquí se agrega a la actividad, el Fragmento que contiene el detalle del item.
            //Además se pasa el argumento idItem.
            Bundle arguments = new Bundle();
            arguments.putInt(BookDetailFragment.posicionLibro,getIntent().getIntExtra(BookDetailFragment.posicionLibro, 0));
            BookDetailFragment fragment = new BookDetailFragment();
            fragment.setArguments(arguments);
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.fragDetalleItem, fragment)
                    .commit();
        }
    }

    public void mostrarMensajeAlerta(Context context, String titulo, String mensaje){
        AlertDialog.Builder builder = new AlertDialog.Builder(context);

        builder.setMessage(mensaje)
                .setTitle(titulo);

        builder.setPositiveButton("ACEPTAR", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
            }
        });
        AlertDialog alertDialog=builder.create();
        alertDialog.show();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            //cuando se presione el boton regresar, entonces se mostrará la animacion
            //mostrando a nueva pantalla desde arriba hacia abajo.
            overridePendingTransition(R.anim.trasladar_to_y_delta_0, R.anim.trasladar_to_y_delta_100);
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
