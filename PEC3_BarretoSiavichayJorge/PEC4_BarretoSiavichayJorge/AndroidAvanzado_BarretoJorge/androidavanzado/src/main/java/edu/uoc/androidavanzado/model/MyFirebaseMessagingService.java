package edu.uoc.androidavanzado.model;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.AudioAttributes;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import edu.uoc.androidavanzado.BookListActivity;
import edu.uoc.androidavanzado.R;

/*
Es el servicio encargado de procesar los mensajes (notificaciones) que llegan desde Firebase.
*/
public class MyFirebaseMessagingService extends FirebaseMessagingService {

    //Establece variables para manejar (administrar) y mostrar las notificaciones recibidas.
    NotificationManager notificationManager;
    Notification notification;

    //Variables que identifican las acciones que se podrá realizar sobre la notificación recibida
    private static final String VER_DETALLE = "ver_detalle";
    private static final String ELIMINAR_DE_LISTA = "eliminar_de_lista";

    //Variable que identifica la posición en la que se encuentra el libro que se va a eliminar o ver su detalle.
    public static final String posicionLibro = "posicionLibro";

    //Método que se ejecuta cuando llegan los mensajes desde firebase.
    @Override
    public void onMessageReceived(@NonNull RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        /*si llega una notificacion, entonces obtiene el título y cuerpo o mensaje.
        Además, verifica si existen datos y especialmente si existe un dato con nombre o clave "book_position"
        que tiene como valor la posición del libro al que se le aplicará la acción seleccionada.
        Por último se en envía el título, mensaje y posicion del libro (si existe) como valores de los parámetros para generar la notificación y visualizar en pantalla.
         */
        if(remoteMessage.getNotification()!=null){
            String titulo=remoteMessage.getNotification().getTitle();
            String mensaje=remoteMessage.getNotification().getBody();
            if(remoteMessage.getData().size()>0){
                String book_position=remoteMessage.getData().get("book_position");
                generarNotificacion(titulo,mensaje, book_position);
                Log.d("posicion libro: ",book_position+"");
            }
            else{
                generarNotificacion(titulo,mensaje, "");
            }
        }

    }

    //Genera la notificación según los datos pasados como parámetros y muestra la notificación en el dispositivo.
    public void generarNotificacion(String titulo, String mensaje, String posicion){
        /*Se prepara un intent que se va a llamar una activity cuando se selecciones la acción "ver detalle" en la notificación.
         Al intent se pasa la posicion del libro y la accion que se seleccionó. En este la accion de "ver detalle del libro".
        */
        Intent intent1 = new Intent(MyFirebaseMessagingService.this, BookListActivity.class);
        intent1.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent1.setAction(VER_DETALLE);
        intent1.putExtra(posicionLibro,posicion);
        PendingIntent verDetalleIntent= PendingIntent.getActivity(MyFirebaseMessagingService.this,0, intent1, PendingIntent.FLAG_ONE_SHOT);
        /*Se prepara un intent que se va a llamar una activity cuando se selecciones la acción "eliminar" en la notificación.
         Al intent se pasa la posicion del libro y la accion que se seleccionó. En este la accion de "ver detalle del libro".
         */
        Intent intent2 = new Intent(MyFirebaseMessagingService.this, BookListActivity.class);
        intent2.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent2.setAction(ELIMINAR_DE_LISTA);
        intent2.putExtra(posicionLibro,posicion);
        PendingIntent eliminarDeListaIntent= PendingIntent.getActivity(MyFirebaseMessagingService.this,0, intent2, PendingIntent.FLAG_ONE_SHOT);

        /*
        Define el sonido que se reproducirá al recibir la notificación
        En este caso, si se recibió como posición un número par tendrá el sonido
        "TYPE_RINGTONE" y si es una posición impar se reproducirá el sonido "TYPE_ALARM".
        Si la supuesta posición no es un número, entonces se reproducirá el sonido de notificación por defecto (TYPE_NOTIFICATION).
         */
        Uri urlSonido= null;
        try {
            int posicionNumero = Integer.parseInt(posicion);
            if (posicionNumero % 2 == 0) {
                urlSonido=RingtoneManager.getDefaultUri(RingtoneManager.TYPE_RINGTONE);
            } else {
                urlSonido=RingtoneManager.getDefaultUri(RingtoneManager.TYPE_ALARM);
            }
        }
        catch(NumberFormatException n) {
            urlSonido=RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        }


        /*Aquí se construye la notificación que visualizará el usuario.
        Si el nivel de API del dispositvio es mayor o igual a la version 26, entonces se crea un
        canal de notificación y luego se construye la notificación como tal, pero si tiene una version
        menor, entonces no se crea el canal de comunicación porque no es requerido.
        */
        if(Build.VERSION.SDK_INT>=26){
            /*
            Crea el canal de notificación con un id y nombre de canal.
            Además brinda visibilidad cuando se la pantalla del dispositivo esté bloqueado
             y establece vibración con un patrón especificado cuando llega la notificación.
             */
            String idCanal="edu.uoc.androidavanzado.model";
            String nombreCanal="Canal";
            NotificationChannel notificationChannel=new NotificationChannel(idCanal,nombreCanal,NotificationManager.IMPORTANCE_DEFAULT);
            notificationChannel.setLockscreenVisibility(Notification.VISIBILITY_PUBLIC);
            notificationChannel.enableVibration(true);
            notificationChannel.setVibrationPattern(new long[]{1000,1000});

            //Establece los atributos de audio y agrega el sonido a reproducir al recibir la notificación.
            AudioAttributes audioAttributes = new AudioAttributes.Builder()
                    .setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION)
                    .setUsage(AudioAttributes.USAGE_NOTIFICATION)
                    .build();
            notificationChannel.setSound(urlSonido,audioAttributes);

            //Inicializa el manejador de notificaciones incluyendo el canal de notificación creado anteriormente
            notificationManager= (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            assert notificationManager!=null;
            notificationManager.createNotificationChannel(notificationChannel);

            /*
            Construye la notificación y propiedades que tendrá que visualizar en la pantalla
            Em este caso se coloca una imagen de libro como ícono de la notificación, establece un
            título, mensaje, el sonido, la vibración y las acciones que podrá seleccionar el usuario (Ver detalle y eliminar).
            */
            NotificationCompat.Builder notificationBuilder= new NotificationCompat.Builder(MyFirebaseMessagingService.this,idCanal);
            //notificationBuilder.setOngoing(true)
            notificationBuilder.setSmallIcon(R.drawable.book)
                    .setLargeIcon(BitmapFactory.decodeResource(getResources(),R.drawable.book))
                    .setContentTitle(titulo)
                    .setContentText(mensaje)
                    //.setTicker(mensaje)
                    .setColor(Color.BLUE)
                    .setAutoCancel(true)
                    .setSound(urlSonido)
                    .setVibrate(new long[] { 1000, 1000 })
                    .addAction(new NotificationCompat.Action(R.drawable.ver_detalle, "Ver Detalle", verDetalleIntent))
                    .addAction(new NotificationCompat.Action(R.drawable.delete, "Eliminar", eliminarDeListaIntent));
                    //.setContentIntent(pendingIntent);

            //Construye la notificación con las propiedades definidas anteriormente y visualiza en la pantalla
            notification=notificationBuilder.build();
            notificationManager.notify(0,notification);

        }
        else{
            //CUANDO LA VERSION DEL API ES MENOR A LA 26

            //Inicializa el manejador de notificaciones sin un canal de notificación
            notificationManager= (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

            /*
            Construye la notificación y propiedades que tendrá que visualizar en la pantalla
            Em este caso se coloca una imagen de libro como ícono de la notificación, establece un
            título, mensaje, el sonido, la vibración y las acciones que podrá seleccionar el usuario (Ver detalle y eliminar).
            */
            Notification.Builder nb=new Notification.Builder(MyFirebaseMessagingService.this);
            nb.setSmallIcon(R.drawable.book);
                    nb.setLargeIcon(BitmapFactory.decodeResource(getResources(),R.drawable.book));
                    nb.setContentTitle(titulo);
                    nb.setContentText(mensaje);
                    //nb.setContentIntent(pendingIntent);
                    nb.setAutoCancel(true);
                    nb.setSound(urlSonido);
                    nb.setVibrate(new long[]{ 1000, 1000 });
                    nb.addAction(R.drawable.ver_detalle, "Ver Detalle", verDetalleIntent);
                    nb.addAction(R.drawable.delete, "Eliminar", eliminarDeListaIntent);

                    nb.build();

            //Construye la notificación con las propiedades definidas anteriormente y visualiza en la pantalla
            notification=nb.getNotification();
            notification.flags = Notification.FLAG_AUTO_CANCEL;
            notificationManager.notify(0,notification);
        }


    }

    //devolución de llamada de onNewToken se activa cuando se genera un token nuevo.
    @Override
    public void onNewToken(String token) {
        Log.d("TOKENNNN", "token: " + token);
    }
}
